---
title: Notes — carefully written.
layout: hextra-home
---

<div class="mt-6 mb-6">
{{< hextra/hero-headline >}}
  Notes, carefully written<br class="sm:block hidden" />by me to me.
{{< /hextra/hero-headline >}}
</div>

<div class="mb-12">
{{< hextra/hero-subtitle >}}
  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
  <br class="sm:block hidden" />
  incididunt ut labore et dolore magna aliqua. Bibendum ut tristique et egestas quis
  <br class="sm:block hidden" />
  ipsum suspendisse. Imperdiet dui accumsan sit amet.
{{< /hextra/hero-subtitle >}}
</div>

{{< hextra/feature-grid >}}
  {{< hextra/feature-card
    title="Docs"
    icon="book-open"
    subtitle="Documentation description goes here. For the moment there is none, it's a feature not a bug."
  >}}
  {{< hextra/feature-card
    title="Blog"
    icon="annotation"
    subtitle="Yet another placeholder description. The blog will be quite empty at the start, but I'll try to force myself to write some thoughts."
  >}}
{{< /hextra/feature-grid >}}
